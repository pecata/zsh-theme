# zsh theme

My custom oh-my-zsh theme

## Install
```
git clone https://gitlab.com/pecata/zsh-theme.git
ln -s $(pwd)/zsh-theme/pecata.zsh-theme ~/.oh-my-zsh/custom/themes
```
Thne change `~/.zshrc` plugin to `pecata`