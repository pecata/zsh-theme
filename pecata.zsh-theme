local red='%{\001\033[1;31m\002%}'
local green='%{\001\033[1;32m\002%}'
local yellow='%{\001\033[1;33m\002%}'
local cyan='%{\001\033[1;36m\002%}'
local white='%{\001\033[0;37m\002%}'

git_status_calc() {
  # echo -en "${white}"
  echo -en "%F{white}"
  branch=$(git branch 2> /dev/null | awk '/^\*/ {print substr($0, 3) }')
  if [ $? -ne 0 ] || [ ${#branch} -eq 0 ]; then
    echo -en "$(print -rD $PWD) "
    return 0
  fi

  # PWD
  local parents=$(dirname "$(git rev-parse --show-toplevel)")
  local base=$(basename "$(git rev-parse --show-toplevel)")
  
  print -nrD $parents
  echo -en "${green}"
  echo -en "/${base}"
  echo -en "${cyan}"
  echo -en "${PWD/${parents}\/${base}/}"
  # end of PWD

  local separator="${yellow}/"

  echo -en " ${yellow}(${branch}"

  # ag=head, behind, count
  local commit_all_ahe=$(git log --oneline @{u}.. 2> /dev/null | wc -l | tr -d ' ')
  local commit_all_beh=$(git log --oneline ..@{u} 2> /dev/null | wc -l | tr -d ' ')

  if [ "${commit_all_beh}${commit_all_ahe}" -gt 0 ]
  then
    echo -en "${yellow}"
    echo -en " {"
    echo -en "${green}"
    echo -en "+${commit_all_ahe}"
    echo -en "${separator}"
    echo -en "${red}"
    echo -en "${commit_all_beh}"
    echo -en "${yellow}"
    echo -en "}"
  fi

  echo -en "${yellow}) ["
  echo -en "${green}"
  echo -en "$(git diff --cached --numstat | wc -l | tr -d '[:space:]')"
  echo -en "${separator}"
  echo -en "${cyan}"
  echo -en "$(git diff --numstat | wc -l | tr -d '[:space:]')"
  echo -en "${separator}"
  echo -en "${red}"
  echo -en "$(git ls-files --others --exclude-standard | wc -l | tr -d '[:space:]')"
  echo -en "${yellow}] %{\001\033[0m\002%}"  
}

set-git-status-to-env() {
  export GIT_STATUS_PROMPT="$(git_status_calc)"
}

set-ssh-remote-to-env() {
  if [[ "$(tty)" == *"pts"* ]]
  then
    export SSH_REMOTE_PROMPT="> "
  fi
}

autoload -Uz add-zsh-hook
add-zsh-hook precmd set-git-status-to-env
add-zsh-hook precmd set-ssh-remote-to-env

# Enable/disable the right prompt options.
setopt no_prompt_bang prompt_percent prompt_subst

PROMPT="%F{cyan}\${SSH_REMOTE_PROMPT}%(?.%F{yellow}.%F{red}%? -> )[%D{%f.%m %H:%M:%S}] %(#.%B%F{red}.%F{green})%n%F{cyan}@%m %b\${GIT_STATUS_PROMPT}%% %f"
